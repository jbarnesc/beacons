package winsystems.ibksreader;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.accent_systems.ibks_sdk.EDSTService.ASEDSTService;
import com.accent_systems.ibks_sdk.connections.ASConDeviceCallback;
import com.accent_systems.ibks_sdk.scanner.ASBleScanner;
import com.accent_systems.ibks_sdk.scanner.ASResultParser;
import com.accent_systems.ibks_sdk.scanner.ASScannerCallback;
import com.accent_systems.ibks_sdk.utils.ASUtils;

import java.util.ArrayList;
import java.util.List;

import android.bluetooth.BluetoothGatt;

import org.json.JSONObject;

import winsystems.logger.FileWriter;

public class MainActivity extends AppCompatActivity implements ASScannerCallback, ASConDeviceCallback {

    public final static String EXTRA_MESSAGE = "";

    private static String TAG = "MainActivity";

    private ListView devicesList;
    private ArrayAdapter<String> adapter;

    private List<String> scannedDevicesList;

    List<String> m_allowed_devices;

    static ProgressDialog connDialog;
    private int m_ListeningTime;
    private int m_WaitingTime;
    private int m_ScanMode;

    private Handler m_start_handler;
    private Handler m_stop_handler;
    private boolean m_scan_pressed;
    private boolean m_last_scan_pressed;
    private boolean m_first_time = true;

    private FileWriter m_FileWriter;
    private int m_last_device_rssi;
    private long m_log_time;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_last_scan_pressed = false;

        m_allowed_devices = new ArrayList<>();

        //m_allowed_devices.add("E5:C2:5A:64:0B:1D");
        //m_allowed_devices.add("F4:13:99:ED:F0:88");
       m_allowed_devices.add("EB:44:24:6B:A2:A7");

        //m_allowed_devices.add("C6:21:74:0A:BA:36");
        //m_allowed_devices.add("D4:D6:69:33:F8:3B");
        //m_allowed_devices.add("C5:47:20:C9:3D:0C");



        m_FileWriter = new FileWriter(this);
        m_FileWriter.CreateLog("");
        m_FileWriter.CreateLog("");
        m_FileWriter.CreateLog("Time,Power,Address");
        m_log_time = 0;

        EditText time1 = (EditText) findViewById(R.id.time1);

        time1.setOnFocusChangeListener(new View. OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    EditText time1 = (EditText) findViewById(R.id.time1);
                                    time1.setText("");
                                }
                            });
                    }
                }
        });
        time1.setText("5000");
        EditText time2 = (EditText) findViewById(R.id.time2);
        time2.setOnFocusChangeListener(new View. OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            EditText time2 = (EditText) findViewById(R.id.time2);
                            time2.setText("");
                        }
                    });
                }
            }
        });
        time2.setText("1800");
    }




    public void onPressScan(View view)
    {
        //Intent intent = new Intent(this, BeaconInfoActivity.class);

        EditText time1 = (EditText) findViewById(R.id.time1);
        String _waiting_time = time1.getText().toString();

        EditText time2 = (EditText) findViewById(R.id.time2);
        String _listening_time = time2.getText().toString();

        Spinner scan = (Spinner) findViewById(R.id.scan);
        Integer _scan_mode = scan.getSelectedItemPosition()-1;

        TextView msg_warning = (TextView) findViewById((R.id.label_scan_warning));
        msg_warning.setText("");

        if (_listening_time.matches("") || _waiting_time.matches("") )
        {
            msg_warning.setText(getResources().getString(R.string.msg_data_warning));
            msg_warning.setTextColor(Color.RED);
        }
        else {
            ConfigAPPValues();

            //intent.putExtra(EXTRA_MESSAGE, listening_time + "-" + waiting_time);
            //startActivity(intent);
            msg_warning.setText(getResources().getString(R.string.msg_show_beacons));
            msg_warning.setTextColor(Color.BLACK);
            m_scan_pressed = !m_last_scan_pressed;
            if (InitScan()&& m_first_time)
            {
                if (!Scan())
                {
                    Log.e(TAG, "Scan() Error");
                }
            }
        }
    }

    private boolean InitScan()
    {
        devicesList = (ListView) findViewById(R.id.devicesList);
        scannedDevicesList = new ArrayList<>();

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, scannedDevicesList );

        devicesList.setAdapter(adapter);

        connDialog = new ProgressDialog(MainActivity.this);
        connDialog.setTitle("Please wait...");

         return true;
    }

    public void ConfigAPPValues()
    {
        EditText time1 = (EditText) findViewById(R.id.time1);
        String _waiting_time = time1.getText().toString();

        EditText time2 = (EditText) findViewById(R.id.time2);
        String _listening_time = time2.getText().toString();

        Spinner scan = (Spinner) findViewById(R.id.scan);
        Integer _scan_mode = scan.getSelectedItemPosition()-1;

        if (!_waiting_time.matches(""))
        {
            m_WaitingTime = Integer.parseInt(_waiting_time) * 1000;
        }

        if (!_listening_time.matches(""))
        {
            m_ListeningTime = Integer.parseInt(_listening_time) * 1000;
        }

        m_ScanMode = ScanSettings.SCAN_MODE_LOW_LATENCY;
    }


    private boolean Scan()
    {
        String _msg;
        Integer _scan_settings;
        requestLocationPermissions();
        TextView msg_warning = (TextView) findViewById((R.id.label_scan_warning));

         ConfigAPPValues();
         _scan_settings = m_ScanMode;
         int err;
         // Start scan
         new ASBleScanner(this, this).setScanMode(_scan_settings);
         err = ASBleScanner.startScan();

         if (err != ASUtils.TASK_OK)
         {

             Log.i(TAG, "startScan - Error (" + Integer.toString(err) + ")");

             if (err == ASUtils.ERROR_LOCATION_PERMISSION_NOT_GRANTED)
             {
                requestLocationPermissions();
             }
         }else {
            BluetoothAdapter _adapter = ASBleScanner.getmBluetoothAdapter();
         }

            _msg = "Scanning: yes";
            msg_warning.setText(_msg);
            m_start_handler = new Handler();
            m_start_handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    StopScan();
                }
            }, m_ListeningTime);

            m_log_time = System.currentTimeMillis();
            Handler Time = new Handler();

          /*  Time.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LogWriter();
                }
            }, 1000);*/

        return true;
    }

    private void StopScan()
    {
        TextView _msg_warning;
        String _msg;

        _msg = "Scanning: no";

        ASBleScanner.stopScan();
        m_FileWriter.CreateLog("");
        m_FileWriter.CreateLog("");

        _msg_warning= (TextView) findViewById((R.id.label_scan_warning));
        _msg_warning.setText(_msg);

        m_stop_handler = new Handler();

        if (m_first_time || m_last_scan_pressed == m_scan_pressed)
        {
            m_stop_handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Scan();
                }
            }, m_WaitingTime);
        }
        m_first_time = false;
        m_last_scan_pressed = m_scan_pressed;

    }

    @TargetApi(23)
    public void requestLocationPermissions(){
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    // region IMPLEMENTATION OF ASScannerCallback

    @Override
    // Callback from ASBleScanner
    // This callback is called when a bluetooth packet is received.
    public void scannedBleDevices(ScanResult result){

        String _device_name;
        String _device_address;
        Integer _device_Rssi;
        String _advertisingString;
        String _log_str;

        _advertisingString = ASResultParser.byteArrayToHex(result.getScanRecord().getBytes());
        _log_str = result.getDevice().getAddress()+" / RSSI: "+result.getRssi()+" / Adv packet: "+ _advertisingString;

        // Get device information
        _device_name = result.getDevice().getName();
        _device_address = result.getDevice().getAddress();
        _device_Rssi = result.getRssi();

        //Check if scanned device is already in the list by mac address
        boolean contains = false;

        for (int i = 0; i < scannedDevicesList.size(); i++) {
            if (scannedDevicesList.get(i).contains(_device_address)) {
                //Device already added
                contains = true;
                //Replace the device with updated values in that position
                scannedDevicesList.set(i, "RSSI:" + _device_Rssi + " / Name:" + _device_name + "\nAddress:" + _device_address);// + "\nDistance:" + _device_distance);
                break;
            }
        }


        if(!contains && m_allowed_devices.contains(_device_address)){
            //Scanned device not found in the list. NEW => add to list
            scannedDevicesList.add("RSSI:" + _device_Rssi + " / Name:" + _device_name + "\nAddress:" + _device_address);// + "\nDistance:" + _device_distance);
            contains = true;
        }

        if (contains)
        {
            LogWriter(_device_Rssi,_device_address);
        }

        //After modify the list, notify the adapter that changes have been made so it updates the UI.
        //UI changes must be done in the main thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
        JSONObject advData;
        switch (ASResultParser.getAdvertisingType(result)){
            case ASUtils.TYPE_IBEACON:
                /**** Example to get data from advertising ***/
                 advData = ASResultParser.getDataFromAdvertising(result);
                 try {
                 Log.i(TAG, "FrameType = " +advData.getString("FrameType")+" AdvTxPower = "+advData.getString("AdvTxPower")+" UUID = "+advData.getString("UUID")+" Major = "+advData.getString("Major")+" Minor = "+advData.getString("Minor"));
                 }catch (Exception ex){
                 Log.i(TAG,"Error parsing JSON");
                 }

                Log.i(TAG,result.getDevice().getName()+" - iBEACON - "+ _log_str);
                break;
            case ASUtils.TYPE_EDDYSTONE_UID:
                /**** Example to get data from advertising ***
                 advData = ASResultParser.getDataFromAdvertising(result);
                 try {
                 Log.i(TAG, "FrameType = " +advData.getString("FrameType")+" AdvTxPower = "+advData.getString("AdvTxPower")+" Namespace = "+advData.getString("Namespace")+" Instance = "+advData.getString("Instance"));
                 }catch (Exception ex){
                 Log.i(TAG,"Error parsing JSON");
                 }
                 /*******************************************/
                Log.i(TAG,result.getDevice().getName()+" - UID - "+ _log_str);
                break;
            case ASUtils.TYPE_EDDYSTONE_URL:
                /**** Example to get data from advertising ***
                 advData = ASResultParser.getDataFromAdvertising(ronChangeStatusConnection
                 Log.i(TAG, "FrameType = " +advData.getString("FrameType")+"  AdvTxPower = "+advData.getString("AdvTxPower")+" Url = "+advData.getString("Url"));
                 }catch (Exception ex){
                 Log.i(TAG,"Error parsing JSON");
                 }
                 /*******************************************/
                Log.i(TAG,result.getDevice().getName()+" - URL - "+ _log_str);

                break;
            case ASUtils.TYPE_EDDYSTONE_TLM:
                /**** Example to get data from advertising ***
                 advData = ASResultParser.getDataFromAdvertising(result);
                 try {
                 if(advData.getString("Version").equals("0")){
                 Log.i(TAG, "FrameType = " +advData.getString("FrameType")+" Version = "+advData.getString("Version")+" Vbatt = "+advData.getString("Vbatt")+" Temp = "+advData.getString("Temp")+" AdvCount = "+advData.getString("AdvCount")+" TimeUp = "+advData.getString("TimeUp"));
                 }
                 else{
                 Log.i(TAG, "FrameType = " +advData.getString("FrameType")+" Version = "+advData.getString("Version")+" EncryptedTLMData = "+advData.getString("EncryptedTLMData")+" Salt = "+advData.getString("Salt")+" IntegrityCheck = "+advData.getString("IntegrityCheck"));
                 }
                 }catch (Exception ex){
                 Log.i(TAG,"Error parsing JSON");
                 }
                 /*******************************************/
                Log.i(TAG,result.getDevice().getName()+" - TLM - "+ _log_str);
                break;
            case ASUtils.TYPE_EDDYSTONE_EID:
                /**** Example to get EID in Clear by the air ***
                 if(!readingEID) {
                 readingEID = true;
                 new ASEDSTService(null,this,10);
                 ASEDSTService.setClient_ProjectId(client, getPrefs.getString("projectId", null));
                 ASEDSTService.getEIDInClearByTheAir(result);
                 }
                 /**************************************************/
                /**** Example to get data from advertising ***
                 advData = ASResultParser.getDataFromAdvertising(result);
                 try {
                 Log.i(TAG, "FrameType = " +advData.getString("FrameType")+" AdvTxPower = "+advData.getString("AdvTxPower")+" EID = "+advData.getString("EID"));
                 }catch (Exception ex){
                 Log.i(TAG,"Error parsing JSON");
                 }
                 /*******************************************/
                Log.i(TAG,result.getDevice().getName()+" - EID - "+ _log_str);
                break;
            case ASUtils.TYPE_DEVICE_CONNECTABLE:
                Log.i(TAG,result.getDevice().getName()+" - CONNECTABLE - "+ _log_str);
                break;
            case ASUtils.TYPE_UNKNOWN:
                Log.i(TAG,result.getDevice().getName()+" - UNKNOWN - "+ _log_str);
                break;
            default:
                Log.i(TAG,"ADVERTISING TYPE: "+ "ERROR PARSING");
                break;
        }
    }

    //endregion

    // region IMPLEMENTATION OF ASConDeviceCallback

    @Override
    public void onChangeStatusConnection(int result, BluetoothGatt blgatt){
        switch (result){
            case ASUtils.GATT_DEV_CONNECTED:
                Log.i(TAG,"onChangeStatusConnection - DEVICE CONNECTED: "+blgatt.getDevice().getName());
                break;
            case ASUtils.GATT_DEV_DISCONNECTED:
                Log.i(TAG,"onChangeStatusConnection - DEVICE DISCONNECTED: "+blgatt.getDevice().getName());
                if (connDialog != null && connDialog.isShowing()) {
                    connDialog.dismiss();
                }
                break;
            default:
                Log.i(TAG,"onChangeStatusConnection - ERROR PARSING");
                break;
        }
    }

    public void onWriteDeviceChar(int result, BluetoothGattCharacteristic characteristic) {
        switch (result) {
            case ASUtils.GATT_WRITE_SUCCESSFULL:
                Log.i(TAG, "onWriteDeviceChar - WRITE SUCCESSFULL on: " + characteristic.getUuid().toString() );
                break;
            case ASUtils.GATT_WRITE_ERROR:
                Log.i(TAG, "onWriteDeviceChar - WRITE ERROR on: " + characteristic.getUuid().toString() );
                break;
            default:
                Log.i(TAG, "onWriteDeviceChar - ERROR PARSING");
                break;
        }
    }

    public void onReadDeviceValues(int result, BluetoothGattCharacteristic characteristic, String value){
        switch (result){
            case ASUtils.GATT_READ_SUCCESSFULL:
                Log.i(TAG, "onReadDeviceValues - READ VALUE: " + value);
                break;
            case ASUtils.GATT_READ_ERROR:
                Log.i(TAG, "onReadDeviceValues - READ ERROR");
                break;
            case ASUtils.GATT_NOTIFICATION_RCV:
                Log.i(TAG, "onReadDeviceValues - READ NOTIFICATION: " + value);
                break;
            case ASUtils.GATT_RSSI_OK:
                Log.i(TAG, "onReadDeviceValues - READ RSSI: " + value);
                break;
            case ASUtils.GATT_RSSI_ERROR:
                Log.i(TAG, "onReadDeviceValues - READ RSSI ERROR");
                break;
            default:
                Log.i(TAG, "onReadDeviceValues - ERROR PARSING");
                break;
        }
    }

    public void onServicesCharDiscovered(int result, BluetoothGatt blgatt, ArrayList<BluetoothGattService> services, ArrayList<BluetoothGattCharacteristic> characteristics)
    {
        switch (result){
            case ASUtils.GATT_SERV_DISCOVERED_OK:
                int err;
                Log.i(TAG, "onServicesCharDiscovered - SERVICES DISCOVERED OK: "+blgatt.getDevice().getName());

                /**** Example to read a characteristic ***
                 myCharRead = ASConDevice.findCharacteristic("00002a28");
                 [Window Title]
                 WSI.WWP_Server

                 [Main Instruction]
                 WSI.WWP_Server dejó de funcionar

                 [Content]
                 Windows está buscando una solución al problema...

                 [Cancelar].readCharacteristic(myCharRead);
                 /*****************************************/


                /**** Example to set Eddystone Slots a characteristic ***

                 ASEDSTSlot[] slots = new ASEDSTSlot[4];
                 ASEDSTService.setClient_ProjectId(client,getPrefs.getString("projectId", null));

                 slots[0] = new ASEDSTSlot(ASEDSTDefs.FT_EDDYSTONE_UID,800,-4,-35,"0102030405060708090a0b0c0d0e0f11");
                 slots[1] = new ASEDSTSlot(ASEDSTDefs.FT_EDDYSTONE_EID,950,-4,-35,"1112131415161718191a1b1c1d1e1f200a");
                 slots[2] = new ASEDSTSlot(ASEDSTDefs.FT_EDDYSTONE_URL,650,0,-21,"http://goo.gl/yb6Mgt");
                 slots[3] = new ASEDSTSlot(ASEDSTDefs.FT_EDDYSTONE_TLM,60000,4,-17,null);
                 ASEDSTService.setEDSTSlots(slots);
                 /********************************************************/

                /**** Example to set iBeacon Slots a characteristic ***
                 ASiBeaconSlot[] slotsib = new ASiBeaconSlot[2];
                 slotsib[0] = new ASiBeaconSlot(false,800,0,-21,"01010101010101010101010101010101","0002","0003",false);
                 slotsib[1] = new ASiBeaconSlot(false,400,-8,-40,"01010101010101010101010101010102","0004","0005",true);
                 ASiBeaconService.setiBeaconSlots(slotsib);
                 /******************************************************/


                /*** Example to get EDST or iBeacon Slots ***/
                // ASEDSTService.setClient_ProjectId(client,getPrefs.getString("projectId", null));
                ASEDSTService.getEDSTSlots();
                //ASiBeaconService.getiBeaconSlots();ll
                /********************************************/

                /*** Example to set Characteristics ***
                 ASEDSTService.setActiveSlot(2);
                 //ASEDSTService.setRadioTxPower(-4);
                 //ASiBeaconService.setExtraByte(true);
                 //ASiBeaconService.setUUIDMajorMinor(false,"0102030405060708090a0b0c0d0e0f10","0001","0002");
                 //ASGlobalService.setONOFFAdvertising(9,22);
                 //ASGlobalService.setDeviceName("iBKS-TEST");
                 /*****************************************************/

                /*** Example to get Characteristics ***
                 ASEDSTService.getLockState();
                 //ASiBeaconService.getActiveSlot();
                 //ASGlobalService.getDeviceName();
                 /*****************************************************/

                break;
            case ASUtils.GATT_SERV_DISCOVERED_ERROR:
                Log.i(TAG, "onServicesCharDiscovered - SERVICES DISCOVERED ERROR: "+blgatt.getDevice().getName());
                break;
            default:
                Log.i(TAG, "onServicesCharDiscovered - ERROR PARSING");
                break;
        }
    }
        public void LogWriter()
        {
            Handler _time_handler = new Handler();

            String _time = String.valueOf(System.currentTimeMillis() - m_log_time);

            if (m_last_device_rssi !=  0)
            {
               m_FileWriter.CreateLog(_time + "," + m_last_device_rssi);
            }

            _time_handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LogWriter();
                }
            }, 1000);
        }

        public void LogWriter(Integer Rssi, String Mac)
        {
            String _time = String.valueOf(System.currentTimeMillis() - m_log_time);
            m_FileWriter.CreateLog(_time + "," + Rssi + "," + Mac);
            TextView msg_warning = (TextView) findViewById((R.id.label_scan_warning));
            msg_warning.setText("Time: " + _time);
        }


}
