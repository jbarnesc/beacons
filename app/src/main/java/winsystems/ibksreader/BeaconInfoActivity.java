package winsystems.ibksreader;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.content.Intent;

public class BeaconInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon_info);

        Intent intent = getIntent();
        TextView textview_listening_time = (TextView) findViewById(R.id.listening_time);
        TextView textview_waiting_time = (TextView) findViewById(R.id.waiting_time);
        String[] separated = intent.getStringExtra(MainActivity.EXTRA_MESSAGE).split("-");
        textview_listening_time.setText(separated[0]);
        textview_waiting_time.setText(separated[1]);

    }
}
